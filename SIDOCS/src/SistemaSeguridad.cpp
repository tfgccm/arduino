#include "../include/SistemaSeguridad.h"

SistemaSeguridad::SistemaSeguridad():sPIR(D_PIRPIN, D_PIRLEDPIN),
									sGAS(A_GASPIN, D_GASLEDPIN),
									sAGUA(A_AGUAPIN, D_AGUALEDPIN),
									sDHT(D_DHTPIN, DHTTYPE),
									aZUM(D_ZUMPIN)
{
	modo = 0; // Se inicializa el modo 3 -> Desactivado (Todas las alarmas inactivas)  
	for(int i=0;i<3;i++){
	alarmas[i]=false;
	}
	alarmaPIR = false;
	alarmaGAS = false;
	alarmaAGUA = false;
	caracter_inicial = '#'; // Se inicializa con un caracter distinto a los manejados por el proceso
	recep_parametros = false;
	buffer[longitud_cadena];

}

SistemaSeguridad::~SistemaSeguridad(){
    
    
}

void SistemaSeguridad::inicializar(){
    
	sPIR.inicializar();
	sGAS.inicializar();
	sAGUA.inicializar();
	sDHT.inicializar();
	aZUM.inicializar();

 
}

void SistemaSeguridad::sincronizar(){
  
	// Enviamos el token 'S' por puerto serie para sincronizar la conexión con Raspberry Pi
	Serial.println('S');	
	
}

void SistemaSeguridad::sensor_leer(){
    
	sPIR.leer();
	sGAS.leer();
	sAGUA.leer();
	sDHT.leer();
	
}

void SistemaSeguridad::serie_enviar_datos(){
  
	// Formato completo de envío de la cadena de datos: "T,tt.tt,H,hh.hh,A,a,a,a"
	
	// Envío de Temperatura y Humedad ("T,tt.tt,H,hh.hh,")
	sDHT.enviar(); 
	
	// Envío del estado de Alarmas ("A,a,a,a")
	Serial.print('A'); 
	for(int i=0;i<3;i++){
	Serial.print(',');
	Serial.print(alarmas[i]);
	}
	Serial.print(',');
	Serial.print('$');
	// Fin de cadena
	Serial.println(); 
    
}


void SistemaSeguridad::serie_leer_parametros(){

	// Variable auxiliar para controlar el inicio y el fin de la recepción de parámetros de RBPi
	recep_parametros = true;
    // Puntero auxiliar para almacenar el modo de funcionamiento recibido de RBPi
    char pmodo[1] = {0};
    // char pgiro[1] = {0}; Ángulo de giro del servomotor (evolutivo futuro)
	
			Serial.readBytes(buffer,longitud_cadena);
			for(int i = 0; i<longitud_cadena; i++){
				if(buffer[i] == 'M'){
					pmodo[0] = buffer[i+1];
					modo = atof(pmodo);
				}
				/*else if(buffer[j] == 'G'){
					//Gestión del ángulo de giro del servomotor (evolutivo futuro)
				}*/
				
				// Una vez finalizada la recepcción de parámetros, forzamos la lectura de la consigna de RBPi para
				// emprender el envío de datos
				else if (buffer[i] == '$'){
					while (recep_parametros) {
						caracter_inicial = Serial.read();
						if (caracter_inicial == 'D'){
						//Serial.println("¡D1!");
						serie_enviar_datos();
						recep_parametros = false;
						}		
					}
				break;
				}
			}

			/*while (recep_parametros) {
				
				caracter_inicial = Serial.read();
				if (caracter_inicial == 'D'){
				//Serial.println("¡D1!");
				serie_enviar_datos();
				recep_parametros = false;
				}		
			}*/

}

void SistemaSeguridad::comunicacion_serie(){
	
	
	if (Serial.available() > 0){
	// Se lee el byte de entrada:
		caracter_inicial = Serial.read();
		if (caracter_inicial == 'P'){
		
			serie_leer_parametros();
		}
		else if (caracter_inicial == 'D'){
		
			serie_enviar_datos();
		}
	}
}

void SistemaSeguridad::evaluar(){
  
	switch(modo){
		
		case 1: // Todas las alarmas activadas
			alarmaPIR = sPIR.evaluar();
			alarmaGAS = sGAS.evaluar();
			alarmaAGUA = sAGUA.evaluar();
			
			if (alarmaPIR || alarmaGAS || alarmaAGUA){
			aZUM.sonar();
			}
			alarmas[0]=alarmaPIR;
			alarmas[1]=alarmaGAS;
			alarmas[2]=alarmaAGUA;
			break;
			
		case 2: // Alarma PIR inactiva
			sPIR.reset(); 
			alarmaGAS = sGAS.evaluar();
			alarmaAGUA = sAGUA.evaluar();
			
			if (alarmaGAS || alarmaAGUA){
			aZUM.sonar();
			}
			alarmas[0]=false;
			alarmas[1]=alarmaGAS;
			alarmas[2]=alarmaAGUA;	
			break;

			
		case 3: // Todas las alarmas inactivas (por definir en evolutivos futuros)
			sPIR.reset();
			sGAS.reset();
			sAGUA.reset();
			
			alarmas[0]=false;
			alarmas[1]=false;
			alarmas[2]=false;
			break;
		
		default:
			sPIR.reset();
			sGAS.reset();
			sAGUA.reset();
			
			alarmas[0]=false;
			alarmas[1]=false;
			alarmas[2]=false;
			break;
	}
}

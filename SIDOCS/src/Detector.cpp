#include "../include/Detector.h"


Detector::Detector(uint8_t pPIR,uint8_t pLED){
  pin=pPIR;
  pinLED=pLED;
  alarma=false;
  
}

Detector::~Detector(){
    
    
    
}

void Detector::inicializar(){
    
  pinMode(pin, INPUT);
  pinMode(pinLED, OUTPUT);
    
}

void Detector::leer(){
    

    
}

bool Detector::evaluar(){


   
}

void Detector::reset(){
	
	// Forzamos el apagado del LED y asignamos a false el atributo movimiento
	digitalWrite(pinLED, LOW);
	alarma=false;
}

#include "../include/DetectorInundacion.h"



DetectorInundacion::DetectorInundacion(uint8_t pAGUA,uint8_t pAGUALED):Detector(pAGUA,pAGUALED){
    
	//pinLED=pLED;
	//pin=pPIR;
	lecturaA=0;
	lecturaB=0;
	valor = 0;
	//inundacion=false;
}

DetectorInundacion::~DetectorInundacion(){    
    
}

/*void DetectorInundacion::inicializar(){
    
	pinMode(pin, INPUT);
	pinMode(pinLED, OUTPUT);  
    
}*/

void DetectorInundacion::leer(){
    
    // Con la primera lectura "lecturaA" y el posterior delay,
    // se pueden realizar multiples lecturas analógicas de distintos sensores en el mismo ciclo.
    lecturaA = analogRead(pin);		
    delay(100);						
    lecturaB = analogRead(pin);
    
    
    valor = map(lecturaB, 1000,118,0,100);
	
	if (valor<0) valor = 0;
	if (valor>100) valor = 100;
	
	
}

bool DetectorInundacion::evaluar(){

	// Si se detecta presencia de inundación
   if (valor >= 25 ){ 
	// LED indicador encendido
	digitalWrite(pinLED, HIGH); 
	alarma=true;      
   } 
   else{
	// LED indicador apagado
	digitalWrite(pinLED, LOW);     
	alarma=false;
   }
     // Se retorna el valor del atributo que controla la presencia de inundación  
	return alarma;
}

/*void DetectorInundacion::reset(){
	
	// Forzamos el apagado del LED y asignamos a false el atributo movimiento
	digitalWrite(pinLED, LOW);    
	inundacion=false;
}*/

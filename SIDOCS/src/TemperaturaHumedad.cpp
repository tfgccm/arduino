#include "../include/TemperaturaHumedad.h"


TemperaturaHumedad::TemperaturaHumedad(uint8_t pDHT,uint8_t tDHT):dht(pDHT, tDHT){

  pin=pDHT;
  tipo=tDHT;  
  hum=0.0f;
  temp=0.0f;
  hic=0.0f;
}

TemperaturaHumedad::~TemperaturaHumedad(){}

void TemperaturaHumedad::inicializar(){
  
  dht.begin();
  
}

void TemperaturaHumedad::leer(){
  
  hum = dht.readHumidity();
  temp = dht.readTemperature();
}

void TemperaturaHumedad::enviar(){

  // Se controla si se ha leído corretamente la temperatura y la humedad, retornando '1' si uno de ellos no es un valor numérico
  if (isnan(hum) || isnan(temp)){
  return;
  }
  else{
  
  Serial.print("T");
  Serial.print(',');
  Serial.print(temp);
  Serial.print(',');
  Serial.print("H");
  Serial.print(',');
  Serial.print(hum);
  Serial.print(",");
 }
 
}

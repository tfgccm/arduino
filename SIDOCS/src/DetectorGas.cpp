#include "../include/DetectorGas.h"



DetectorGas::DetectorGas(uint8_t pGAS, uint8_t pGASLED):Detector(pGAS,pGASLED){
   
  //pin=pGAS;
  //pinLED=pLED; 
  lecturaA=0;
  lecturaB=0;
  valor=0;
  //fuga=false;  
 
}

DetectorGas::~DetectorGas(){
    
}

/*void DetectorGas::inicializar(){

  pinMode(pin, INPUT);
  pinMode(pinLED, OUTPUT);	
  
}*/

void DetectorGas::leer(){
   
   
    // Con la primera lectura "lecturaA" y el posterior delay,
    // se pueden realizar multiples lecturas analógicas de distintos sensores en el mismo ciclo.
    lecturaA = analogRead(pin);		
    delay(100);						
    lecturaB = analogRead(pin);
    
    //Serial.print("El valor del sensor MQ2 obtenido es: ");
    //Serial.println(lecturaB);
    
    //nivel = map(lecturaB, 200,685,0,100);
	valor = map(lecturaB, 150,550,0,100);
	
	// Al trabajar sobre %, se fuerza que el nivel del gas/humo no pueda exceder el 100% ni ser menor a 0%
	if (valor<0) valor = 0;
	if (valor>100) valor = 100;

}

bool DetectorGas::evaluar(){

	// Si se detecta presencia de gas
    if (valor >= 50){ 
	  // LED indicador encendido
      digitalWrite(pinLED, HIGH);  
	  alarma=true;
   }
   	// Si no se detecta presencia de gas
   else{
	  // LED indicador apagado
      digitalWrite(pinLED, LOW);
	  alarma=false;
   }
   // Se retorna el valor del atributo que controla la presencia de gas
   return alarma;
   
}

/*void DetectorGas::reset(){
	
	// Forzamos el apagado del LED y asignamos a false el atributo fuga
	digitalWrite(pinLED, LOW);
	fuga=false;
}*/

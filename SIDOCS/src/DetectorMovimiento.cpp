#include "../include/DetectorMovimiento.h"



DetectorMovimiento::DetectorMovimiento(uint8_t pPIR,uint8_t pPIRLED):Detector(pPIR,pPIRLED){
  //pin=pPIR;
  //pinLED=pLED;
  valor=LOW;
  //movimiento=false;
  
}

DetectorMovimiento::~DetectorMovimiento(){
    
    
    
}

/*void DetectorMovimiento::inicializar(){
    
  pinMode(pin, INPUT);
  pinMode(pinLED, OUTPUT);
    
}*/

void DetectorMovimiento::leer(){
    
  valor = digitalRead(pin);
    
}

bool DetectorMovimiento::evaluar(){

  // Si se detecta presencia de movimiento
  if (valor == HIGH){ 
	  
	// LED indicador encendido
	digitalWrite(pinLED, HIGH);
	alarma=true;
  }
  // Si no se detecta presencia de movimiento
  else{
	// LED indicador apagado
	digitalWrite(pinLED, LOW); // LED OFF
	alarma=false;
  }
  // Se retorna el valor del atributo que controla la presencia de movimiento
  return alarma;
   
}

/*void DetectorMovimiento::reset(){
	
	// Forzamos el apagado del LED y asignamos a false el atributo movimiento
	digitalWrite(pinLED, LOW);
	movimiento=false;
}*/

#include "../include/Sirena.h"


Sirena::Sirena(uint8_t pZUM){
    
  pin=pZUM;
  nivel = 0;
}

Sirena::~Sirena(){
    
}

void Sirena::inicializar(){
    
  pinMode(pin, OUTPUT); 
}

void Sirena::sonar(){
	
  // Parámetros de emisión del zumbador: frecuencia = 1000Hz, duración = 2s
  tone(pin, 1000,2000); 
}

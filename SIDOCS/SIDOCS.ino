// Incluimos librería de SistemaSeguridad
#include "include/SistemaSeguridad.h"
SistemaSeguridad sidocs;

void setup() {

  // Inicializamos comunicación serie
  Serial.begin(9600);

  // Inicializamos y asignamos el valor de las conexiones a los sensores
  sidocs.inicializar();
  delay(1000);
  sidocs.sincronizar();

}

void loop() {

  // Lectura del estado de los sensores
  sidocs.sensor_leer();
  // Generación de alarmas en función del modo de funcionamiento 
  sidocs.evaluar();
  // Obtención del modo de funcionamiento / envío de datos al servidor   
  sidocs.comunicacion_serie(); 
  delay(2500);
}





#include <DHT.h>


class TemperaturaHumedad{
  
  private:
  
  uint8_t pin;
  uint8_t tipo;
  float hum;
  float temp;
  float hic;
  
  public:
  DHT dht; 
  TemperaturaHumedad(uint8_t ,uint8_t ); 
  virtual ~TemperaturaHumedad();
  void inicializar();
  void leer();
  void enviar();
   

};

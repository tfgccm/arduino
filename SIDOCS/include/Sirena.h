#include "Arduino.h"

class Sirena {
  
  private:
  //uint8_t pinLED;
  uint8_t pin;
  int nivel;
  
  public:
  Sirena (uint8_t);
  virtual ~Sirena();
  void inicializar();
  void sonar();

 
 
};

#ifndef __DETECTOR_H__
#define __DETECTOR_H__

#include "Arduino.h"

class Detector {
  
  protected:
  uint8_t pinLED;
  uint8_t pin;
  int valor;
  bool alarma;
  
  public:
  Detector (uint8_t, uint8_t);
  virtual ~Detector();
  void inicializar();
  void leer();
  bool evaluar();
  void reset();

};


#endif

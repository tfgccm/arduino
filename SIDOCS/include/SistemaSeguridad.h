#include "../include/DetectorInundacion.h"
#include "../include/DetectorMovimiento.h"
#include "../include/DetectorGas.h"
#include "../include/TemperaturaHumedad.h"
#include "../include/Sirena.h"

// Definición de I/O digitales de los sensores -- (Analógico/Digital)_(Nombre Sensor)({LED de control})PIN

const uint8_t D_DHTPIN = 2;
const uint8_t D_PIRPIN = 3;
const uint8_t D_ZUMPIN = 5;
const uint8_t D_PIRLEDPIN = 8;
const uint8_t D_AGUALEDPIN = 9;
const uint8_t D_GASLEDPIN = 10;

// Definición de I/O analógicos de los sensores
const uint8_t A_GASPIN = A0;
const uint8_t A_AGUAPIN = A1;

// Definición del tipo de sensor de DHT
//const uint8_t DHTTYPE = DHT11;

const uint8_t DHTTYPE = DHT22;

//Longitud de cadena de recepción de parámetros por puerto serie.
const byte longitud_cadena = 6; 

class SistemaSeguridad {
  
  private:
  
  int modo; // Modo 1 -> Completo (Todas las alamas activas), Modo 2 -> En casa (Alarma PIR inactiva), Modo 3 -> Desactivado (Todas las alarmas inactivas)
  boolean alarmas[3]; // Vector de alarmas: alarmas[0] -> Agua, alarmas[1] -> Gas, alarmas[2] -> Movimiento
  boolean alarmaPIR;
  boolean alarmaGAS;
  boolean alarmaAGUA;

  char buffer[longitud_cadena];

  public:
  boolean recep_parametros;
  boolean env_datos;
  

  DetectorMovimiento sPIR;
  DetectorInundacion sAGUA;
  DetectorGas sGAS;
  TemperaturaHumedad sDHT;
  Sirena aZUM;
  
  char caracter_inicial;
  SistemaSeguridad();
  virtual ~SistemaSeguridad();
  void inicializar();
  void sincronizar();
  void evaluar();
  void serie_leer_parametros();
  void serie_enviar_datos();
  void comunicacion_serie();
  void sensor_leer();
  int getModo(){ return modo;}
  void setModo(int m){ modo = m;}


 
 
};
